package com.cenas.standings.ws.persistence.repo;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import com.cenas.standings.ws.persistence.model.PlayerEntity;


public interface PlayerRepository extends PagingAndSortingRepository<PlayerEntity, Long>{
	

	PlayerEntity findByDci( Long dci);
	
	List<PlayerEntity> findByName(@Param("name") String name);

}
