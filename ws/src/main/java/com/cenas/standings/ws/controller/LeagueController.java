package com.cenas.standings.ws.controller;


import java.util.List;
import java.util.Optional;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.cenas.standings.ws.dto.League;
import com.cenas.standings.ws.dto.PageableResource;
import com.cenas.standings.ws.dto.Resource;
import com.cenas.standings.ws.dto.Tournament;
import com.cenas.standings.ws.persistence.model.LeagueEntity;
import com.cenas.standings.ws.persistence.model.TournamentEntity;
import com.cenas.standings.ws.persistence.repo.LeagueRepository;
import com.cenas.standings.ws.persistence.repo.TournamentRepository;

@CrossOrigin(maxAge = 3600, origins="http://localhost:4200")
@RestController
@RequestMapping("/api/leagues")
public class LeagueController extends SpringBeanAutowiringSupport {
	
	private static final int MAX_PAGE_SIZE = 20;

	@Autowired
	private LeagueRepository leagueRepo;
	
	@Autowired
	private TournamentRepository tournamentRepo;
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces="application/json")
	public ResponseEntity<Resource> getLeague( @PathVariable("id") long id) {
		Optional<LeagueEntity> opt = leagueRepo.findById(id);
		
		if(opt.isPresent()) {
			LeagueEntity ent = opt.get();
			League l = new League();
			l.setId(ent.getId());
			l.setName(ent.getName());
			l.setFormat(ent.getFormatCode());
			l.setStart(ent.getStart());
			l.setEnd(ent.getEnd());
			return new ResponseEntity<>(l, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		
	}
	
	@RequestMapping(method = RequestMethod.GET, produces="application/json")
	public ResponseEntity<PageableResource<League>> getLeagues(@RequestParam(value="page", required=false) Integer page, @RequestParam(value="size", required=false) Integer size) {
		
		PageRequest pageReq;
		
		int pageSize;
		int pageNumber;
		
		if(page == null)
			pageNumber = 0;
		else
			pageNumber = page;
		
		if(size == null || size > 20)
			pageSize = MAX_PAGE_SIZE;
		else
			pageSize= size;
		
		long total = leagueRepo.count();
		pageReq = PageRequest.of(pageNumber, pageSize);
		
		Page<LeagueEntity> opt = leagueRepo.findAll(pageReq);
		ArrayList<League> ret = new ArrayList<>(opt.getSize());
		
		for(LeagueEntity ent : opt) {
			League l = new League();
			l.setId(ent.getId());
			l.setName(ent.getName());
			l.setFormat(ent.getFormatCode());
			l.setStart(ent.getStart());
			l.setEnd(ent.getEnd());
			ret.add(l);
		}
		
		PageableResource<League> p = new PageableResource<League>(ret, total);
		
		return new ResponseEntity<>(p, HttpStatus.ACCEPTED);
	}
	
	@RequestMapping(method = RequestMethod.POST, produces="application/json")
	public ResponseEntity<Resource> createLeague(@RequestBody League league) {
		
		LeagueEntity ent = new LeagueEntity();
		ent.setName(league.getName());
		ent.setFormatCode(league.getFormat());
		ent.setStart(league.getStart());
		ent.setEnd(league.getEnd());
		
		try{
			leagueRepo.save(ent);
		} catch(Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<>(HttpStatus.ACCEPTED);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces="application/json")
	public ResponseEntity<Resource> deleteLeague(@PathVariable("id") long id) {
		
		if(leagueRepo.existsById(id)) {
			leagueRepo.deleteById(id);
			return new ResponseEntity<>(HttpStatus.ACCEPTED);
		} else {
			Resource res = new Resource("404", "League resource with id" + id +  " not found.", "A liga escolhida n�o foi encontrada.");
			return new ResponseEntity<>(res, HttpStatus.NOT_FOUND);
		}
		
	}

	@RequestMapping(value = "/{id}/tournaments", method = RequestMethod.GET, produces="application/json")
	public ResponseEntity<PageableResource<Tournament>> getLeagueTournaments(@PathVariable("id") long leagueId, @RequestParam(value="page", required=false) Integer page, @RequestParam(value="size", required=false) Integer size) {
		
		/*PAGING*/
		PageRequest pageReq;
		
		int pageSize;
		int pageNumber;
		
		if(page == null)
			pageNumber = 0;
		else
			pageNumber = page;
		
		if(size == null || size > 20)
			pageSize = MAX_PAGE_SIZE;
		else
			pageSize= size;
		
		pageReq = PageRequest.of(pageNumber, pageSize);
		
		
		/*QUERY*/
		Optional<LeagueEntity> l = leagueRepo.findById(leagueId);
		
		if( !l.isPresent() ) {
			PageableResource<Tournament> err = new PageableResource<Tournament>("404", "League with id " + leagueId + " does not exist.", "A liga escolhida n�o foi encontrada.");
			return new ResponseEntity<>(err, HttpStatus.NOT_FOUND);
		}
		
		Long count = tournamentRepo.countByFormat(l.get().getFormatCode());
		List<TournamentEntity> ents = tournamentRepo.findByFormat(l.get().getFormatCode(), pageReq);
		
		
		/*RETURN*/
		List<Tournament> ret = new ArrayList<Tournament>(ents.size());
		for(TournamentEntity ent : ents) {
			Tournament t = new Tournament();
			t.setId(ent.getId());
			t.setName(ent.getName());
			t.setFormat(ent.getFormat());
			t.setDate(ent.getDate());
			
			ret.add(t);
		}
		
		PageableResource<Tournament> p = new PageableResource<Tournament>(ret, count);
		
		return new ResponseEntity<>(p, HttpStatus.ACCEPTED);
	}
	
	
	
	
	

}
