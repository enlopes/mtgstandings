package com.cenas.standings.ws.persistence.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(schema = "mtgstandings", name="tournaments")
public class TournamentEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tournament_id_seq")
	@SequenceGenerator(name="tournament_id_seq", sequenceName = "tournament_id_seq", allocationSize = 1, schema="mtgstandings")
	private Long id;
	
	@Column(name="name", nullable=false)
	private String name;
	
	@Column(name="date", nullable=false)
	private LocalDateTime date;
	
	
	@Column(name="format_code", nullable=false)
	private String format;
	
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}


	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}
	
}
