package com.cenas.standings.ws.persistence.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(schema = "mtgstandings", name="leagues")
public class LeagueEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "league_id_seq")
	@SequenceGenerator(name="league_id_seq", sequenceName = "league_id_seq", allocationSize = 1, schema="mtgstandings")
	private Long id;
	
	@Column(name="name", nullable=false)
	private String name;
	
	@Column(name="start_date")
	private LocalDate start;
	
	@Column(name="end_date")
	private LocalDate end;
	
	@Column(name="format_code")
	private String formatCode;

	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getStart() {
		return start;
	}

	public void setStart(LocalDate start) {
		this.start = start;
	}

	public LocalDate getEnd() {
		return end;
	}

	public void setEnd(LocalDate end) {
		this.end = end;
	}

	public String getFormatCode() {
		return formatCode;
	}

	public void setFormatCode(String formatCode) {
		this.formatCode = formatCode;
	}

}
