package com.cenas.standings.ws.persistence.repo;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.cenas.standings.ws.persistence.model.TournamentEntity;

public interface TournamentRepository extends JpaRepository<TournamentEntity, Long> {
	
	@Query("SELECT t FROM TournamentEntity t WHERE t.format LIKE ?1%")
	List<TournamentEntity> findByFormat(String format, Pageable p);
	
	@Query("SELECT count(t) FROM TournamentEntity t WHERE t.format LIKE ?1%")
	Long countByFormat(String format);
	
}
