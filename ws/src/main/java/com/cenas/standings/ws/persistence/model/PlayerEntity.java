package com.cenas.standings.ws.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema = "mtgstandings", name="players")
public class PlayerEntity {

	@Id
	private Long id;
	
	@Column(name="name")
	private String name;
	
	
	@Column(name="dci", unique=true)
	private Long dci;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getDci() {
		return dci;
	}

	public void setDci(Long dci) {
		this.dci = dci;
	}
	
	
	
}
