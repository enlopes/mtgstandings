package com.cenas.standings.ws.dto;

import java.time.LocalDateTime;


public class Tournament extends Resource {

	private Long id;
	
	private String name;
	
	private LocalDateTime date;
	
	private String format;

	
	
	public Tournament() {
		super();
	}

	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}
	
	
	
	
}
