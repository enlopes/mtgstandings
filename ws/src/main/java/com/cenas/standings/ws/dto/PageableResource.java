package com.cenas.standings.ws.dto;

import java.util.List;

/**
 * Includes information that may be useful for front-end pagination.
 * 
 * @author Eduardo Nave Lopes
 *
 */
public class PageableResource<T> extends Resource {
	
	/**
	 * The total number of elements of the query (ignoring paging information).
	 * May be used to calculate the total number of pages without requesting all elements.
	 */
	private Long total;
	
	private List<T> elements;
	
	/**
	 * Success constructor.
	 * @param total The total number o elements of the query (ignoring pages)
	 */
	public PageableResource(List<T> elements, Long total) {
		super();
		this.total = total;
		this.elements = elements;
	}
	
	/**
	 * Error constructor
	 * @param error Error code
	 * @param devMsg Message for developers. May contain implementation details.
	 * @param userMsg Message for users. May be shown to front-end users
	 */
	public PageableResource(String error, String devMsg, String userMsg) {
		super(error,devMsg,userMsg);
	}

	
	
	public Long getTotal() {
		return total;
	}

	
	public List<T> getElements() {
		return elements;
	}
}
