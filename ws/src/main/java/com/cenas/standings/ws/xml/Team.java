package com.cenas.standings.ws.xml;

import javax.xml.bind.annotation.XmlAttribute;

public class Team {
	
	
	private int rank;
	
	private String name;
	
	private String dci;
	
	private int matchPoints;
	
	private String matchResultsPWDB;
	
	private double opponentMatchWinPercent;
	
	private double gamesWinPercent;
	
	private double opponentsGameWinPercent;

	
	
		
	public int getRank() {
		return rank;
	}
	
	@XmlAttribute(name = "Rank")
	public void setRank(int rank) {
		this.rank = rank;
	}

	
	
	public String getName() {
		return name;
	}

	@XmlAttribute(name = "Name")
	public void setName(String name) {
		this.name = name;
	}

	
	
	public String getDci() {
		return dci;
	}

	@XmlAttribute(name = "DCI")
	public void setDci(String dci) {
		this.dci = dci;
	}

	
	
	public int getMatchPoints() {
		return matchPoints;
	}

	@XmlAttribute(name = "MatchPoints")
	public void setMatchPoints(int matchPoints) {
		this.matchPoints = matchPoints;
	}

	
	
	public String getMatchResultsPWDB() {
		return matchResultsPWDB;
	}

	@XmlAttribute(name = "MatchResultsPWDB")
	public void setMatchResultsPWDB(String matchResultsPWDB) {
		this.matchResultsPWDB = matchResultsPWDB;
	}

	
	
	public double getOpponentMatchWinPercent() {
		return opponentMatchWinPercent;
	}

	@XmlAttribute(name = "OpponentMatchWinPercent")
	public void setOpponentMatchWinPercent(double opponentMatchWinPercent) {
		this.opponentMatchWinPercent = opponentMatchWinPercent;
	}

	
	
	public double getGamesWinPercent() {
		return gamesWinPercent;
	}

	@XmlAttribute(name = "GamesWinPercent")
	public void setGamesWinPercent(double gamesWinPercent) {
		this.gamesWinPercent = gamesWinPercent;
	}

	
	
	public double getOpponentsGameWinPercent() {
		return opponentsGameWinPercent;
	}

	@XmlAttribute(name = "OpponentsGameWinPercent")
	public void setOpponentsGameWinPercent(double opponentsGameWinPercent) {
		this.opponentsGameWinPercent = opponentsGameWinPercent;
	}
	
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		
		//rank
		sb.append("\"rank\":");
		sb.append(rank);
		sb.append(',');
		
		//name
		sb.append("\"name\":");
		sb.append('"');
		sb.append(name);
		sb.append('"');
		sb.append(',');
		
		//dci
		sb.append("\"dci\":");
		sb.append('"');
		sb.append(dci);
		sb.append('"');
		sb.append(',');
		
		sb.append("}");
		return sb.toString();
	}
	
	
}
