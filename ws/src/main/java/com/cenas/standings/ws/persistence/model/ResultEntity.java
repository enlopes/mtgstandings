package com.cenas.standings.ws.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(schema = "mtgstandings", name="results")
public class ResultEntity {

	@Id
	private Long id;
	
	@ManyToOne(optional = true)
	@JoinColumn(name = "player_id")
	private PlayerEntity player;
	
	@ManyToOne(optional = true)
	@JoinColumn(name = "tournament_id")
	private TournamentEntity tournament;
	
	@Column(name="played")
	private Integer played;
	
	@Column(name="win")
	private Integer win;
	
	@Column(name="draw")
	private Integer draw;
	
	@Column(name="bye")
	private Integer bye;

	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PlayerEntity getPlayer() {
		return player;
	}

	public void setPlayer(PlayerEntity player) {
		this.player = player;
	}

	public TournamentEntity getTournament() {
		return tournament;
	}

	public void setTournament(TournamentEntity tournament) {
		this.tournament = tournament;
	}

	public Integer getPlayed() {
		return played;
	}

	public void setPlayed(Integer played) {
		this.played = played;
	}

	public Integer getWin() {
		return win;
	}

	public void setWin(Integer win) {
		this.win = win;
	}

	public Integer getDraw() {
		return draw;
	}

	public void setDraw(Integer draw) {
		this.draw = draw;
	}

	public Integer getBye() {
		return bye;
	}

	public void setBye(Integer bye) {
		this.bye = bye;
	}
	
	
}
