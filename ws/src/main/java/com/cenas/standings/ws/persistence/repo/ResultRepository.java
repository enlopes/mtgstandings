package com.cenas.standings.ws.persistence.repo;


import org.springframework.data.repository.PagingAndSortingRepository;

import com.cenas.standings.ws.persistence.model.ResultEntity;


public interface ResultRepository extends PagingAndSortingRepository<ResultEntity, Long> {


}
