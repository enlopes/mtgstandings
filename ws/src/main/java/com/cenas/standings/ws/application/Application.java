package com.cenas.standings.ws.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages={"com.cenas.standings.ws.controller"})
@EnableJpaRepositories(basePackages="com.cenas.standings.ws.persistence.repo")
@EntityScan(basePackages="com.cenas.standings.ws.persistence.model")
public class Application {
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
