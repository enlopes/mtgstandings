package com.cenas.standings.ws.xml;

import java.util.Set;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Standings")
public class Standings {
	
	
	private Set<Team> teams;

	
	
	public Set<Team> getTeams() {
		return teams;
	}

	@XmlElement( name = "Team", nillable = false, required = true )
	public void setTeams(Set<Team> teams) {
		this.teams = teams;
	}
	
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append('{');
		for(Team t : teams) {
			sb.append(t);
			sb.append(',');
		}
		sb.append('}');
		return sb.toString();
	}

}
