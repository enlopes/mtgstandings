package com.cenas.standings.ws.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;


/**
 * Serves as a parent for all resource classes.
 * This class may only be instantiated in case of error, since the success case would originate an empty response.
 * 
 * @author Eduardo Nave Lopes
 *
 */
@JsonInclude(value=Include.NON_NULL)
public class Resource {
	
	private String errorCode;
	private String devMessage;
	private String userMessage;
	
	/**
	 * Success constructor. Should only called by subclasses.
	 */
	protected Resource() {	}
	
	/**
	 * Error constructor. 
	 * 
	 * @param error Error code
	 * @param devMsg Message for developers. May contain implementation details.
	 * @param userMsg Message for users. May be shown to front-end users
	 */
	public Resource(String errorCode, String devMessage, String userMessage) {
		this.errorCode = errorCode;
		this.devMessage = devMessage;
		this.userMessage = userMessage;
	}
	
	public String getErrorCode() {
		return errorCode;
	}
	
	
	public String getDevMessage() {
		return devMessage;
	}

	public String getUserMessage() {
		return userMessage;
	}
	
	

}
