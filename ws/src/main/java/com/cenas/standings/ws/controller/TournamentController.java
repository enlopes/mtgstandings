package com.cenas.standings.ws.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.cenas.standings.ws.dto.Resource;
import com.cenas.standings.ws.dto.Tournament;
import com.cenas.standings.ws.persistence.model.TournamentEntity;
import com.cenas.standings.ws.persistence.repo.TournamentRepository;

@CrossOrigin(maxAge = 3600, origins="http://localhost:4200")
@RestController
@RequestMapping("/api/tournaments")
public class TournamentController extends SpringBeanAutowiringSupport {

	private static final int MAX_PAGE_SIZE = 20;
	
	@Autowired
	private TournamentRepository tournamentRepo;
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces="application/json")
	public ResponseEntity<Tournament> getTournament( @PathVariable("id") long id) {
		Optional<TournamentEntity> opt = tournamentRepo.findById(id);
		
		if(opt.isPresent()) {
			TournamentEntity ent = opt.get();
			Tournament t = new Tournament();
			t.setId(ent.getId());
			t.setName(ent.getName());
			t.setFormat(ent.getFormat());
			t.setDate(ent.getDate());
			
			return new ResponseEntity<Tournament>(t, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		
	}
	
	@RequestMapping(method = RequestMethod.GET, produces="application/json")
	public ResponseEntity<List<Tournament>> getTournaments(@RequestParam(value="page", required=false) Integer page, @RequestParam(value="size", required=false) Integer size) {
		
		PageRequest pageReq;
		
		int pageSize;
		int pageNumber;
		
		if(page == null)
			pageNumber = 0;
		else
			pageNumber = page;
		
		if(size == null || size > 20)
			pageSize = MAX_PAGE_SIZE;
		else
			pageSize= size;
		
		
		pageReq = PageRequest.of(pageNumber, pageSize);
		
		Page<TournamentEntity> opt = tournamentRepo.findAll(pageReq);
		ArrayList<Tournament> ret = new ArrayList<>(opt.getSize());
		
		for(TournamentEntity ent : opt) {
			Tournament t = new Tournament();
			t.setId(ent.getId());
			t.setName(ent.getName());
			t.setFormat(ent.getFormat());
			t.setDate(ent.getDate());
			ret.add(t);
		}
		
		return new ResponseEntity<>(ret, HttpStatus.ACCEPTED);
	}
	
	@RequestMapping(method = RequestMethod.POST, produces="application/json")
	public ResponseEntity<Resource> createTournament(@RequestBody Tournament tournament) {
		
		TournamentEntity ent = new TournamentEntity();
		ent.setName(tournament.getName());
		ent.setFormat(tournament.getFormat());
		ent.setDate(tournament.getDate());
		try{
			tournamentRepo.save(ent);
		} catch(Exception e) {
			e.printStackTrace();
			Resource r = new Resource("500", "Uncaught exception when saving resource. Check server log for trace.", "Ocorreu um erro a gravar o torneio. Por favor contacte o administrador.");
			return new ResponseEntity<>(r, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<>(HttpStatus.ACCEPTED);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces="application/json")
	public ResponseEntity<Resource> deleteTournament(@PathVariable("id") long id) {
		
		if(tournamentRepo.existsById(id)) {
			tournamentRepo.deleteById(id);
			return new ResponseEntity<>(HttpStatus.ACCEPTED);
		} else {
			Resource r = new Resource("404","Tournament resource with id " + id + " not found.","O torneio escolhido n�o foi encontrado.");
			return new ResponseEntity<>(r, HttpStatus.NOT_FOUND);
		}
		
	}
	
	
	
}
