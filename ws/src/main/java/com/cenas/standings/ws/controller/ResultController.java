package com.cenas.standings.ws.controller;

import java.io.IOException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import org.springframework.web.multipart.MultipartFile;

import com.cenas.standings.ws.persistence.model.PlayerEntity;
import com.cenas.standings.ws.persistence.model.ResultEntity;
import com.cenas.standings.ws.persistence.repo.PlayerRepository;
import com.cenas.standings.ws.persistence.repo.ResultRepository;
import com.cenas.standings.ws.persistence.repo.TournamentRepository;
import com.cenas.standings.ws.xml.Standings;
import com.cenas.standings.ws.xml.Team;

@RestController
public class ResultController extends SpringBeanAutowiringSupport {
	
	@Autowired
	private ResultRepository resultRepo;
	
	@Autowired
	private PlayerRepository playerRepo;
	

	@Autowired
	private TournamentRepository tournamentRepo;
	
	@RequestMapping(method=RequestMethod.POST,name="newResults")
	public String newResults(@PathVariable Long tournamentId, @RequestBody MultipartFile file) {
		
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(Standings.class);
			
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			Standings standings = (Standings) jaxbUnmarshaller.unmarshal(file.getInputStream());
			
			for(Team t : standings.getTeams()) {
				
				String name = t.getName();
				String dciStr = t.getDci(); //long?
				
				Long dci = Long.parseLong(dciStr);
				
				//player
				PlayerEntity p = playerRepo.findByDci(dci);
				if(p == null) {
					p = new PlayerEntity();
					p.setDci(dci);
					p.setName(name);
					p = playerRepo.save(p);
				}
				
				ResultEntity res = new ResultEntity();
				
				//TODO: validar se tournament existe??
				
				//score
				String[] pwdb = t.getMatchResultsPWDB().split("/");
				if(pwdb.length != 4) {
					System.err.println("Erro PWD no dci " + dci + ": " + t.getMatchResultsPWDB());
				} else {
					Integer played = Integer.parseInt(pwdb[0]);
					Integer wins = Integer.parseInt(pwdb[1]);
					Integer draws = Integer.parseInt(pwdb[2]);
					Integer byes = Integer.parseInt(pwdb[3]);
					
					res.setBye(byes);
					res.setDraw(draws);
					res.setPlayed(played);
					res.setPlayer(p);
					res.setTournament(tournamentRepo.getOne(tournamentId));
					res.setWin(wins);
					
					
				}
				
				
				resultRepo.save(res);
			}
			
			
			
			
					
		} catch (JAXBException e) {
//			redirectAttributes.addFlashAttribute("message",
//	                "O ficheiro " + file.getOriginalFilename() + " está corrupto.");
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		return "redirect:/";
	}

}
