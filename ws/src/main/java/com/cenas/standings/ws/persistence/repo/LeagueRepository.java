package com.cenas.standings.ws.persistence.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cenas.standings.ws.persistence.model.LeagueEntity;

public interface LeagueRepository extends JpaRepository<LeagueEntity, Long> {

	
}
