import { Component, OnInit } from '@angular/core';
import {League} from "../../models/league";
import { StorageService } from '../../services/storage.service'

@Component({
  selector: 'app-create-league',
  templateUrl: './create-league.component.html',
  styleUrls: ['./create-league.component.css']
})
export class CreateLeagueComponent implements OnInit {

  league: League;

  constructor(private storage: StorageService) { }

  ngOnInit() {
    this.league = new League('','','','');
  }

  onSubmit(){
    console.log(this.league);
    this.storage.createLeague(this.league).subscribe( data => {
      console.log(data);
    })
  }

}
