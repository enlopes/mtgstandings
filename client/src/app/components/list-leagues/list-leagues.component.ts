import { Component, OnInit } from '@angular/core';
import {League} from '../../models/league';
import {StorageService} from '../../services/storage.service';

@Component({
  selector: 'app-list-leagues',
  templateUrl: './list-leagues.component.html',
  styleUrls: ['./list-leagues.component.css']
})
export class ListLeaguesComponent implements OnInit {

  leagues: League[];

  constructor(private storage: StorageService) { }

  ngOnInit() {
    this.storage.getLeagues().subscribe(data => {
      this.leagues = JSON.parse(data.toString());
    });
  }

}
