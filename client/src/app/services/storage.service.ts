import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {League} from '../models/league';
import {HttpHeaders} from '@angular/common/http';

@Injectable()
export class StorageService {

  constructor(private http: HttpClient) { }

	createLeague(league: League) {
  	const headers = new HttpHeaders({
		'Content-Type': 'application/json',
      'Access-Control-Allow-Origin' : '*'
    });
    const httpOptions = {
      headers: headers
    };
    return this.http.post('http://localhost:8080/leagues', league, httpOptions);
  }

  getLeagues() {
	return this.http.get('/leagues');
  }
}
