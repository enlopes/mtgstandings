export class League {
  constructor(
    public name: string,
    public start: string,
    public end: string,
    public format: string,
  ){}
}
