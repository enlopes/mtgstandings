import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { RouterModule, Routes} from '@angular/router';

import { StorageService } from './services/storage.service';
import { AppComponent } from './app.component';
import { CreateLeagueComponent } from './components/create-league/create-league.component';
import { ListLeaguesComponent } from './components/list-leagues/list-leagues.component';

const appRoutes: Routes = [
	{ path: 'leaguelisting', component: ListLeaguesComponent },
	{ path: 'createleague',      component: CreateLeagueComponent },
	{ path: '',
		redirectTo: '/leaguelisting',
		pathMatch: 'full'
	}
];

@NgModule({
  declarations: [
    AppComponent,
    CreateLeagueComponent,
    ListLeaguesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
      RouterModule
  ],
  providers: [StorageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
